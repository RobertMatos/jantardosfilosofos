package jantarFilosofos;

public class Filosofo extends Thread {

	private int Id;

	final int PENSANDO = 0;
	final int ESFOMEADO = 1;
	final int COMENDO = 2;
	public static int contador[] = new int[5];
	
	public Filosofo(String nome, int Id) {
		super(nome);
		this.Id = Id;
	}

	public void ComFome() {
		
		Grade.estado[this.Id] = 1;

		System.out.println("O filosofo " + getName() + " está esfomeado!");
	}

	public void Come() {

		Grade.estado[this.Id] = 2;

		contador[this.Id]++;
		
		System.out.println("O Filosofo " + getName() + " está comendo! contador:" + contador[this.Id]);

		try {
			Thread.sleep(1000L);
		} catch (InterruptedException ex) {
			System.out.println("ERROR>" + ex.getMessage());
		}
	}

	public void Pensa() {

		Grade.estado[this.Id] = 0;

		System.out.println("O Filosofo " + getName() + " está pensando!");

		try {
			Thread.sleep(1000L);
		} catch (InterruptedException ex) {
			System.out.println("ERROR>" + ex.getMessage());
		}

	}

	public void LargarGarfo() {

		Grade.mutex.decrementar();

		Pensa();

		Grade.filosofo[VizinhoEsquerda()].TentarObterGarfos();
		Grade.filosofo[VizinhoDireita()].TentarObterGarfos();

		Grade.mutex.incrementar();
	}

	public void PegarGarfo() {
		
		Grade.mutex.decrementar();

		ComFome();

		TentarObterGarfos();

		Grade.mutex.incrementar();
		Grade.semaforos[this.Id].decrementar();
	}

	public void TentarObterGarfos() {

		if (Grade.estado[this.Id] == 1 && Grade.estado[VizinhoEsquerda()] != 2 && Grade.estado[VizinhoDireita()] != 2) {
			
			Come();
			
			Grade.semaforos[this.Id].incrementar();
		}

	}

	@Override
	public void run() {
		try {
			Pensa();

			do {
				PegarGarfo();
				Thread.sleep(1000L);
				LargarGarfo();
			} while (true);

		} catch (InterruptedException ex) {
			System.out.println("ERROR>" + ex.getMessage());

			return;
		}
	}

	public int VizinhoDireita() {
		return (this.Id + 1) % 5;
	}

	public int VizinhoEsquerda() {
		if (this.Id == 0) {
			return 4;
		} else {
			return (this.Id - 1) % 5;
		}
	}
}
